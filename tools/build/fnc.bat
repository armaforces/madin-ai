@echo off
call:%~1 %2 %3 %4 %5 %6 %7
goto exit

:build_folder
    setlocal EnableDelayedExpansion
    set mod=@%mod_base%

    if not exist "%mod%\keys" mkdir "%mod%\keys"
    copy /Y %public_key% "%mod%\keys\" > nul && echo Public key copied

    if not exist "%mod%\addons" mkdir "%mod%\addons"
    del /f /s /q "%mod%\addons" 1>nul

    copy /Y mod.cpp "%mod%\" > nul && echo mod.cpp copied
    copy /Y logo.paa "%mod%\" > nul && echo logo.paa copied

    echo.
    echo ==== Build ====
    for /D %%i in (".\%~1\*") do (
        set pbo=%mod_base%_%%~ni.pbo
        echo [Building] !pbo! from %%i
        call "%~dp0\armake2.exe" build -x *.tga -x *.png -x *.md -i P:\ "%%i" ".\%mod%\addons\!pbo!"

        if !errorlevel! neq 0 (
            echo [!pbo!] Failed
            exit /B !errorlevel!
        ) else (
            echo [!pbo!] Ok
        )
        echo.
    )

    if not %sign% == 0 (
        if exist "%private_key%" (
            echo ==== Sign ====
            for %%p in (.\%mod%\addons\*.pbo) do (
                if %use_dssign% == 1 (
                    echo [DSSigning] %%p
                    call :dssign .\%private_key% "%%p"
                ) else (
                    echo [Signing] %%p
                    call "%~dp0\armake2.exe" sign .\%private_key% "%%p"
                )

                if !errorlevel! neq 0 (
                    echo Failed
                    exit /B !errorlevel!
                ) else (
                    echo Ok
                )
                echo.
            )
        ) else (
            if %sign% == 2 (
                echo Can't sign without key: %private_key%
                exit /B 1
            )
            echo Key not found: %private_key%
            echo Skipping signing.
        )
    )

    if %publish% == 1 (
        echo ==== Publish ====
        call :publisher update /id:%workshop_id% /changeNote:"Automatic mod upload" /path:"%cd%\%mod%"

        if !errorlevel! neq 0 (
            REM Error codes: https://community.bistudio.com/wiki/Publisher
            echo Publishing failed, error: !errorlevel!
            exit /B !errorlevel!
        )
        echo Ok
        echo.
    )

    echo [Finished]
goto:eof

:dssign
    set dssign_reg=REG QUERY "HKEY_CURRENT_USER\Software\Bohemia Interactive\dssignfile" /v "PATH"
    %dssign_reg% >nul 2>&1 && (goto :dssign_sign)

    exit /B 1
    :dssign_sign
        for /f "tokens=2,*" %%a in ('%dssign_reg% ^| findstr "PATH"') do (
            set dssign_exe=%%b\DSSignFile.exe
        )

    call "%dssign_exe%" %1 %2
goto:eof

:publisher
    set publisher_reg=REG QUERY "HKEY_CURRENT_USER\Software\Bohemia Interactive\publisher" /v "PATH"
    %publisher_reg% >nul 2>&1 && (goto :publisher_exec)

    exit /B 1
    :publisher_exec
        for /f "tokens=2,*" %%a in ('%publisher_reg% ^| findstr "PATH"') do (
            set publisher_exe=%%b\PublisherCmd.exe
        )

    call "%publisher_exe%" %1 %2 %3 %4 %5 %6
goto:eof

:argparse
    echo                                        ______
    echo        /\                             ^|  ____^|
    echo       /  \    _ __  _ __ ___    __ _  ^| ^|__  ___   _ __  ___  ___  ___
    echo      / /\ \  ^| '__^|^| '_ ` _ \  / _` ^| ^|  __^|/ _ \ ^| '__^|/ __^|/ _ \/ __^|
    echo     / ____ \ ^| ^|   ^| ^| ^| ^| ^| ^|^| (_^| ^| ^| ^|  ^| (_) ^|^| ^|  ^| (__^|  __/\__ \
    echo    /_/    \_\^|_^|   ^|_^| ^|_^| ^|_^| \__,_^| ^|_^|   \___/ ^|_^|   \___^|\___^|^|___/
    echo.
    echo.

    set sign=1
    if "%~1" == "-sign" (
        set sign=2
    )
    if "%~1" == "-nosign" (
        set sign=0
    )
    set use_dssign=0
    if "%~1" == "-ds" (
        set sign=2
        set use_dssign=1
    )
    set publish=0
    if "%~1" == "-publish" (
        set sign=2
        set publish=1
    )
goto:eof

:exit
exit /b
