@echo off
call tools\build\fnc.bat :argparse %1 %2 %3 %4 %5 %6 %7

:: Possible arguments:
::
:: "-sign"      Require sign, script will fail if private key is not present
:: "-nosign"    Do not sign
:: "-ds"        Use DSSign instead of armake2 for signing
:: "-publish"    Update addon on steam workshop
::
:: Mod settings
set private_key=AF_MadinAI.biprivatekey
set public_key=AF_MadinAI.bikey
set mod_base=MadinAI
set workshop_id=1797999135

:: Build folders
call tools\build\fnc.bat :build_folder addons

exit /B %errorlevel%
